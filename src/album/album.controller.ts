import { Controller, Get, Post, Body, HttpCode } from '@nestjs/common';
import { Album } from './interfaces/album.model';
import { AlbumService } from './album.service';
@Controller('album')
export class AlbumController {
  constructor(private albumService: AlbumService) {}

  @Get('list')
  @HttpCode(200)
  async findAll(): Promise<Album[]> {
    console.log('请求 list');
    return this.albumService.findAll();
  }
}
