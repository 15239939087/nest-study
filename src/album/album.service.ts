import { Injectable } from '@nestjs/common';
import { Album } from './interfaces/album.model';

@Injectable()
export class AlbumService {
  private readonly album: Album[] = [];

  create(album: Album) {
    this.album.push(album);
  }

  findAll(): Album[] {
    return this.album;
  }
}
